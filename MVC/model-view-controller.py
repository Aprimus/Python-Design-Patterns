class Model:
	def logic(self):
		data = 'return some data'
		print('Model: Crunching data....')
		return data


class View:
	def  update(self, data):
		print('View: Displaying the view with results: ', data)


class Controller:
	def __init__(self):
		self.model = Model()
		self.view = View()

	def interface(self):
		print('Controller: Responding to client asks...')
		data = self.model.logic()
		self.view.update(data)


class Client:
	def __init__(self):
		self.controller = Controller()

	def make_request(self):
		self.controller.interface()

c = Client()
c.make_request()
