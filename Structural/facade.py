class IntelligentRemoteControl:
	def __init__(self):
		self.television = Television()
		self.radio = Radio()
		self.air_condition = AirCondition()

	def start(self):
		self.television.on()
		self.radio.on()
		self.air_condition.on()


class Television:

	def __init__(self):
		pass

	def on(self):
		print('TV is On')


class Radio:

	def __init__(self):
		pass

	def on(self):
		print('Radio is On')


class AirCondition:

	def __init__(self):
		pass

	def on(self):
		print('AC is ON')


i = IntelligentRemoteControl()

i.start()
