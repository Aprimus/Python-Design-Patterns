class Ceo:
	def __init__(self):
		self.is_busy = False

	def occupied(self):
		self.is_busy = True
		print('CEO is Occupied')

	def available(self):
		self.is_busy = True
		print('CEO is available')

	def get_status(self):
		return self.is_busy


# Proxy
class ContactCenter:
	def __init__(self):
		self.ceo = Ceo()

	def work(self):
		if self.ceo.get_status():
			self.ceo.occupied()

		else:
			self.ceo.available()


c = ContactCenter()
c.work()
