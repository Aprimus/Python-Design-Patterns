class Subject:
    def __init__(self):
        self.observers = []
        self._name = 'Subject name'

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value
        self.notify_observers()

    def add_observer(self, observer):
        self.observers.append(observer)

    def notify_observers(self):
        for obs in self.observers:
            obs.get_notification()


class Observer:
    def __init__(self, subject):
        subject.add_observer(self)

    def get_notification(self):
        print('{0} say Somethings is changed on the subject'.format(self.__class__.__name__))


class Observer1(Observer):
    pass


class Observer2(Observer):
    pass

s = Subject()

o1 = Observer1(s)
o2 = Observer2(s)

s.name = 'Another name'
