# Receiver
class Document:
	def __init__(self, filename):
		self.filename = filename
		self.contents = 'this file cannot be modified'

	def save(self):
		with open(self.filename, 'w') as file:
			file.write(self.contents)


# Invokers
class MenuItem:
	def click(self):
		self.command()


class KeyboardShortcut:
	def keypress(self):
		self.command()

document = Document('filename.txt')
menu_item = MenuItem()
menu_item.command = document.save

keyboard_shortcut = KeyboardShortcut()
keyboard_shortcut.command = document.save

keyboard_shortcut.keypress()



