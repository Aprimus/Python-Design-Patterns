from abc import ABCMeta, abstractmethod


class Trip(metaclass=ABCMeta):
	"""Abstract class"""
	@abstractmethod
	def set_transport(self):
		pass

	@abstractmethod
	def day_1(self):
		pass

	@abstractmethod
	def day_2(self):
		pass

	@abstractmethod
	def day_3(self):
		pass

	@abstractmethod
	def return_home(self):
		pass

	def itinerary(self):
		self.set_transport()
		self.day_1()
		self.day_2()
		self.day_3()
		self.return_home()


class VeniceTrip(Trip):
	"""Concrete Class"""

	def set_transport(self):
		print('Take a boat')

	def day_1(self):
		print('Venice Trip day 1')

	def day_2(self):
		print('Venice Trip day 2')

	def day_3(self):
		print('Venice Trip day 3')

	def return_home(self):
		print('Go back Home')


class MaldiveTrip(Trip):
	"""Concrete Class"""

	def set_transport(self):
		print('On foot')

	def day_1(self):
		print('Maldive Trip day 1')

	def day_2(self):
		print('Maldive Trip day 2')

	def day_3(self):
		print('Maldive Trip day 3')

	def return_home(self):
		print('Return Home from Maldive')

v = VeniceTrip()
v.itinerary()  # Inherit from base class


