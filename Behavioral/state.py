import itertools


class MixinState:
	def __init__(self):
		self.stations = itertools.cycle(self._frequencies)

	def get_next_station(self):
		return next(self.stations)

	def __str__(self):
		return self._name


class FMState(MixinState):
	_frequencies = ['83.3fm', '101.2fm', '103.4fm']
	_name = 'FM'

	def change_frequency(self, radio):
		print('Changing frequency to AM')
		radio.state = AMState()


class AMState(MixinState):
	_frequencies = ['65.7am', '90.4am', '200.6am']
	_name = 'AM'

	def change_frequency(self, radio):
		print('Changing frequency to FM')
		radio.state = FMState()


class Radio:
	def __init__(self):
		self.state = AMState()

	def get_current_station(self):
		return self.state.get_next_station()

	def toggle_AM_FM(self):
		self.state.change_frequency(self)

radio = Radio()
print(radio.state)  # AM
print(radio.get_current_station())  # 65.7am

radio.toggle_AM_FM()  # Changing frequency to FM
print(radio.state)  # FM
print(radio.get_current_station())  # 83.3fm

