import types


class Strategy:
	def __init__(self, func=None):
		if func is not None:
			self.execute = types.MethodType(func, self)  # Plug strategy bound to self

	def execute(self):
		print('Default Strategy')


def execute_replacement1(self):
	print('Replacement1 method {}'.format(self))


def execute_replacement2(self):
	print('Replacement1 method for {0}'.format(self))

# Select a strategy at runtime

s0 = Strategy()
s0.execute()  # default strategy

s1 = Strategy(func=execute_replacement1)
s1.execute()  # replacement 1

s2 = Strategy(func=execute_replacement2)
s2.execute()  # replacement 2
