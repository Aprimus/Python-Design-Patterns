from abc import ABCMeta, abstractmethod


class Cook:
	"""Receiver"""
	def cook_lunch(self):
		print('Cooking Lunch')

	def cook_dinner(self):
		print('Cooking Dinner')


class CommandInterface(metaclass=ABCMeta):
	"""Command Interface"""
	def __init__(self, cook):
		self.cook = cook

	@abstractmethod
	def execute(self):
		raise NotImplemented


class CommandLunch(CommandInterface):
	"""Concrete Command"""
	def execute(self):
		self.cook.cook_lunch()


class CommandDinner(CommandInterface):
	"""Concrete Command"""
	def execute(self):
		self.cook.cook_dinner()


class Customer:
	"""Invoker"""
	def __init__(self, command):
		self.command = command
		self._history = []

	def order(self):
		self._history.append(self.command)
		self.command.execute()

cook = Cook()
command_lunch = CommandLunch(cook)
command_dinner = CommandDinner(cook)

client = Customer(command=command_lunch)
client.order()


