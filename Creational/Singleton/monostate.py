class Borg:
	__shared_state = {'1': '2'}

	def __init__(self):
		self.__dict__ = self.__shared_state
		self.x = 1


class Borg2:
	__shared_state = {}

	def __new__(cls, *args, **kwargs):
		obj = super().__new__(cls, *args, **kwargs)
		obj.__dict__ = cls.__shared_state
		return obj

b = Borg2()
b1 = Borg2()

print(b is b1)  # return False
print(b.__dict__ is b1.__dict__)  # Same object

b.new_attr = 'new_value'

print(hasattr(b1, 'new_attr'))

