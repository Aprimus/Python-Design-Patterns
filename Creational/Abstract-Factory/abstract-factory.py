from abc import ABCMeta, abstractmethod


# Abstract Factory

class AbstractGuitarFactory(metaclass=ABCMeta):
	@abstractmethod
	def create_guitar(self):
		pass

	@abstractmethod
	def create_bass(self):
		pass


# Concrete Factory

class FenderFactory(AbstractGuitarFactory):
	def create_guitar(self):
		return StratoCaster()

	def create_bass(self):
		return JazzBass()


class GibsonFactory(AbstractGuitarFactory):

	def create_guitar(self):
		return LesPaul()

	def create_bass(self):
		return ThunderBird()


# Abstract Products

class Guitar(metaclass=ABCMeta):
	_strings = 6

	@abstractmethod
	def play(self):
		pass


# Fender Concrete Products

class StratoCaster(Guitar):

	def play(self):
		print('Playing like Clapton on my {}, {} string'.format(self.__class__.__name__, self._strings))


class JazzBass(Guitar):
	_strings = 4

	def play(self):
		print('Slapping like Flea on my {}, {} strings'.format(self.__class__.__name__, self._strings))


# Gibson Concrete Products

class LesPaul(Guitar):
	def play(self):
		print('Playing like Jimmy Page on my {}, {} strings'.format(self.__class__.__name__, self._strings))


class ThunderBird(Guitar):
	_strings = 4

	def play(self):
		print('Jamming like Lemmy on my {}, {}'.format(self.__class__.__name__, self._strings))


def abstract_factory():
	for factory in (FenderFactory(), GibsonFactory()):
		product_a = factory.create_guitar()
		product_a.play()
