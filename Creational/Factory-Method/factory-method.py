from abc import ABCMeta, abstractmethod


class AbstractInstrument(metaclass=ABCMeta):

	@abstractmethod
	def play(self):
		pass


class ConcreteInstrument(AbstractInstrument):
	_number_of_strings = 0

	def play(self):
		print('Playing {0} with {1} string'.format(self.__class__.__name__, self._number_of_strings))


class Guitar(ConcreteInstrument):
	_number_of_strings = 6


class BassGuitar(ConcreteInstrument):
	_number_of_strings = 4


class InstrumentFactory:
	@staticmethod
	def create_instrument(instrument):
		"""Factory Method Pattern"""
		if instrument.lower() == 'guitar':
			return Guitar()
		elif instrument.lower() == 'bass':
			return BassGuitar()
		else:
			raise TypeError('Unknow instrument')

factory = InstrumentFactory()
bass_guitar = factory.create_instrument('Bass')

print(bass_guitar.__class__)  # BassGuitar class

bass_guitar.play()
